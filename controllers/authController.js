const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const { authSchema } = require('../validateModels/authValidModels')
require('dotenv').config()

module.exports.register = async (req, res) => {
  try {
    const { email, password, role } = req.body

    const candidate = await User.findOne({email: email})
    if (candidate) {
      return res.status(400).json({message: 'Such a user already exists'})
    }
    const hashedPassword = await bcrypt.hash(password, 12)

    const user = new User({email: email, password: hashedPassword, role: role})
    await user.save()

    res.status(201).json({message: 'User has been created!'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.login = async (req, res) => {
  try {
    const { email, password } = req.body

    const user = await User.findOne({email})
    if(!user) {
      return res.status(404).json({message: 'User was not found!'})
    }

    const isMatch = await bcrypt.compare(password, user.password)
    if(!isMatch) {
      return res.status(400).json({message: 'The password is not correct. Try again!'})
    }

    const jwt_token = jwt.sign(
      {userId: user.id, email: user.email, password: user.password, role: user.role},
      process.env.JWT_SECRET,
      // {expiresIn: '1h'}
    )

    res.json({jwt_token})
    
  } catch (err) {
    res.status(500).json({message: err.message});
  }
}

module.exports.forgotPassword = async (req, res) => {
  try {
    const { email } = req.body

    const user = await User.findOne({ email });
    if(!user) {
      return res.status(404).json({message: 'No users with such email were found!'})
    }
    res.json({message: 'New password sent to your email address'});

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}
