const User = require('../models/User')
const bcrypt = require('bcrypt')


module.exports.getUserInfo = async (req, res) => {
  try {
    const { userId } = req.registeredUser

    const user = await User.findById(userId)
    if(!user){
      return res.status(400).json({message: 'Failed to find'})
    }
    const { _id: id, email, created_date } = user
    res.json({user: {id, email, created_date}})

  } catch(err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.deleteUser = async (req, res) => {
  // User.findByIdAndDelete(req.registeredUser.userId).exec()
  //   .then(() => {
  //     res.json({message: 'Profile deleted successfully'});
  //   })
  //   .catch(err => {
  //     res.status(500).json({error: err.message});
  //   })

  try {
    const { userId } = req.registeredUser

    await User.findByIdAndDelete(userId)

    res.json({message: 'Profile deleted successfully'})
    
  } catch(err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.changeUserPassword = async (req, res) => {
  try {
    const { oldPassword, newPassword } = req.body
    const { password, userId } = req.registeredUser
      
    const validPassword = bcrypt.compare(oldPassword, password)
    
    if (!validPassword) {
      return res.status(400).json({message: 'The passwords hasn\'t been matched!'})
    }

    const hashedPassword = await bcrypt.hash(newPassword, 12)

    const user = await User.findByIdAndUpdate({_id: userId}, {$set: {password: hashedPassword}})
    await user.save()
    
    res.json({message: 'Password changed successfully'})

  } catch(err) {
    res.status(500).json({message: err.message})
  }
}
