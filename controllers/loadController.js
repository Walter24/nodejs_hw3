const Load = require('../models/Load')
const Truck = require('../models/Truck')
const truckTypes = require('../dataOptions/TruckTypes')
const loadStates = require('../dataOptions/LoadStates')


module.exports.addLoad = async (req, res) => {
  try {
    const { userId } = req.registeredUser
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body

    const load = new Load({created_by: userId, name, payload, pickup_address, delivery_address, dimensions})
    if(!load) {
      return res.status(400).json({message: 'Failed to fetch necessary data'})
    }
    await load.save()
    res.status(201).json({message: 'Load created successfully'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.getTheLoads = async (req, res) => {
  try {    
    const { userId, role } = req.registeredUser
    let loads

    if (role === 'SHIPPER') {
      loads = await Load.find({created_by: userId})
    } else if (role === 'DRIVER') {
      loads = await Load.find({assigned_to: userId})
    }

    if(!loads){
      return res.status(404).json({message: 'Your lists haven\'t been found!'})
    }
    res.json([{loads: loads}])

  } catch(err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.getActiveLoadsForDriver = async (req, res) => {
  try {
    const { userId } = req.registeredUser
    
    const activeDriverLoad = await Load.findOne({assigned_to: userId})
    
    if(!activeDriverLoad){
      return res.status(404).json({message: 'Loads for driver were not found!'})
    }
    res.json({activeDriverLoad})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.changeDriverLoadState = async (req, res) => {
  try {
    const { userId } = req.registeredUser

    const load = await Load.findOne({ assigned_to: userId })
    const truck = await Truck.findOne({ assigned_to: userId })

    if (load.status === 'SHIPPED') {
      return res.status(400).json({message: 'The load has already been shipped! You cannot make the changes.'})
    }

    const stateIndex = loadStates.findIndex(state => state === load.state)
    const state = loadStates[stateIndex + 1]
    if (state === 'Arrived to delivery') {
      await load.updateOne({ state, status: 'SHIPPED' })
      await truck.updateOne({ status: 'IS' })
    } else {
      await load.updateOne({ state });
    }
    
    res.json({message: `Load state changed to '${state}'`})

  } catch (err) {
    res.status(500).json({message: err.message})
  }

}

module.exports.getUserLoadById = async (req, res) => {
  try {
    const { userId, role } = req.registeredUser
    const { id } = req.params
    
    const load = await Load.findById(id)
    const createdByShipper = load.created_by
    const assignedToDriver = load.assigned_to

    if (role === 'SHIPPER'){
      if (userId === createdByShipper) {
        res.json({load})
      } else {
        return res.status(404).json({message: 'The load was not created!'})
      }
    } else if (role === 'DRIVER') {
      if (userId === assignedToDriver) {
        res.json({load})
      } else {
        return res.status(404).json({message: 'The load was not assigned!'})
      }
    }

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.updateUserLoadById = async (req, res) => {
  try {
    const { id } = req.params

    const load = await Load.findById(id)
    const { status } = load

    const { ...props } = req.body

    if (status !== 'NEW') {
      return res.status(400).json({message: 'The load has already been processed! The load is not available for update.'})
    }
    await load.updateOne({$set: props})

    res.json({message: 'Load details changed successfully.'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.deleteLoadById = async (req, res) => {
  try {
    const { id } = req.params

    const load = await Load.findById(id)

    const { status } = load

    if (status !== 'NEW') {
      return res.status(400).json({message: 'The load has already been processed! You cannot delete the load.'})
    }
    await load.deleteOne()
    res.json({message: 'Load deleted successfully'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.postLoadById = async (req, res) => {
  try {
    const { id } = req.params // to find id of load

    const load = await Load.findById(id)

    if(!load) {
      return res.status(400).json({message: 'The load has not been found!'})
    }
    const { status, dimensions, payload } = load

    if (status !== 'NEW') {
      return res.status(400).json({message: `The currant status of the load is '${status.toLowerCase()}'`})
    }
    await load.updateOne({$set: {status: 'POSTED'}})

    const assignedTruck = await Truck.find({ status: 'IS'})

    const isMatchedTruck = truckTypes.filter(truck => {
      return truck.payload >= payload
    })
    .filter(truck => {
      for (let size in truck.dimensions) {
        return truck.dimensions[size] >= dimensions[size];
      }
    })
    .map(truck => truck.type)

    const approvedTruck = assignedTruck.find(truck => {
      return isMatchedTruck.find(truckType => {
        return truck.type === truckType
      })
    })

    if (!assignedTruck.length || !approvedTruck) {

      await load.updateOne({status: 'NEW'})

      return res.status(404).json({message: 'Truck was not found!'})
    }

    await load.updateOne({$set: {status: 'ASSIGNED', assigned_to: approvedTruck.assigned_to, state: 'En route to Pick Up'}})
    await approvedTruck.updateOne({$set: {status: 'OL'}})  

    res.json({
      "message": "Load posted successfully",
      "driver_found": true
    })

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.getActiveLoadsById = async (req, res) => {
  try {
    const { id } = req.params // to find id of load

    const load = await Load.findById(id)

    const { assigned_to, status } = load

    if (status === 'NEW') {
      return res.status(400).json({message: 'The load hasn\'t been assigned yet!'})
    }
    const truck = await Truck.findOne({created_by: assigned_to, status: 'OL'})

    res.json({load, truck})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}
