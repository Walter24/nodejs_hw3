const Truck = require('../models/Truck')
const { truckTypeSchema } = require('../validateModels/authTruckVModels')

module.exports.getTruckInfo = async (req, res) => {
  try {
    const { userId } = req.registeredUser

    const trucks = await Truck.find({created_by: userId})
    if(!trucks){
      return res.status(404).json({message: 'Truck was not found!'})
    }
    res.json([{trucks: trucks}])

  } catch(err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.addTruck = async (req, res) => {
  try {
    const { type } = req.body
    const { userId } = req.registeredUser

    try {
      await truckTypeSchema.validateAsync({type})
    } catch (err) {
      return res.status(400).json({message: err.message})
    }

    const truck = new Truck({created_by: userId, type: type, status: 'OS'})
    await truck.save()
    res.status(201).json({message: 'Truck created successfully'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.getTruckById = async (req, res) => {
  try {
    const { id } = req.params

    const truck = await Truck.findById(id)
    if(!truck){
      return res.status(400).json({message: 'Truck with this ID was not found!'})
    }
    const {_id, created_by, assigned_to, type, status, created_date} = truck
    res.json({truck: {_id, created_by, assigned_to, type, status, created_date}})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.updateTruckById = async (req, res) => {
  try {
    const { type } = req.body
    const { id } = req.params

    try {
      await truckTypeSchema.validateAsync({type})
    } catch (err) {
      return res.status(400).json({message: err.message})
    }

    const truck = await Truck.findByIdAndUpdate(id, {$set: {type}})
    if(!truck){
      return res.status(400).json({message: 'Truck was not found!'})
    }
    res.json({message: 'Truck details changed successfully'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.deleteTruck = async (req, res) => {
  try {
    const { id } = req.params

    const truck = await Truck.findByIdAndDelete(id)
    if(!truck){
      return res.status(400).json({message: 'Truck was not found!'})
    }
    await truck.deleteOne()
    res.json({message: 'Truck deleted successfully'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}

module.exports.assignTruckById = async (req, res) => {
  try {
    const { id } = req.params

    const truck = await Truck.findById(id)
    if(!truck){
      return res.status(400).json({message: 'Truck was not found!'})
    }
    const { created_by } = truck
    await truck.updateOne({$set: {assigned_to: created_by, status: 'IS'}})
    res.json({message: 'Truck assigned successfully'})

  } catch (err) {
    res.status(500).json({message: err.message})
  }
}
