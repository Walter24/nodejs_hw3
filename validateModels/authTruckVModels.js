const Joi = require('joi')

const types = [ 'SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT' ]

const truckTypeSchema = Joi.object({
  type: Joi.string()
    .valid(...types)
})

module.exports = {
  truckTypeSchema
}
