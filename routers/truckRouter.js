const express = require('express')
const router = express.Router()

const authMiddleware = require('../middlewares/authMiddleware')
const checkDriverRoleMiddleware = require('../middlewares/checkDriverRoleMiddleware')
const accessMiddleware = require('../middlewares/accessMiddleware')

const { 
  getTruckInfo,
  addTruck ,
  getTruckById,
  updateTruckById,
  deleteTruck,
  assignTruckById
} = require('../controllers/truckController')

router.get('/trucks', authMiddleware, checkDriverRoleMiddleware, getTruckInfo)
router.post('/trucks', authMiddleware, checkDriverRoleMiddleware, addTruck)
router.get('/trucks/:id', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, getTruckById)
router.put('/trucks/:id', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, updateTruckById)
router.delete('/trucks/:id', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, deleteTruck)
router.post('/truck/:id/assign', authMiddleware, checkDriverRoleMiddleware, accessMiddleware, assignTruckById)

module.exports = router