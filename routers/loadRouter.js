const express = require('express')
const router = express.Router()

const authMiddleware = require('../middlewares/authMiddleware')
const checkShipperRoleMiddleware = require('../middlewares/checkShipperRoleMiddleware')
const checkDriverRoleMiddleware = require('../middlewares/checkDriverRoleMiddleware')
const accessMiddleware = require('../middlewares/accessMiddleware')


const { 
  addLoad,
  getTheLoads,
  getActiveLoadsForDriver,
  changeDriverLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteLoadById,
  postLoadById,
  getActiveLoadsById
} = require('../controllers/loadController')

router.get('/loads', authMiddleware, getTheLoads)
router.post('/loads', authMiddleware, checkShipperRoleMiddleware, addLoad)
router.get('/loads/active', authMiddleware, checkDriverRoleMiddleware, getActiveLoadsForDriver)
router.patch('/loads/active/state', authMiddleware, checkDriverRoleMiddleware, changeDriverLoadState)
router.get('/loads/:id', authMiddleware, getUserLoadById)
router.put('/loads/:id', authMiddleware, checkShipperRoleMiddleware, accessMiddleware, updateUserLoadById)
router.delete('/loads/:id', authMiddleware, checkShipperRoleMiddleware, accessMiddleware, deleteLoadById)
router.post('/loads/:id/post', authMiddleware, checkShipperRoleMiddleware, postLoadById)
router.get('/loads/:id/shipping_info', authMiddleware, checkShipperRoleMiddleware, getActiveLoadsById)

module.exports = router
