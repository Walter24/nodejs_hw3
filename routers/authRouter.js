const express = require('express')
const router = express.Router()

const validationsMiddleware = require('../middlewares/validationsMiddleware')

const { register, login, forgotPassword } = require('../controllers/authController')

router.post('/auth/register', validationsMiddleware, register)
router.post('/auth/login', validationsMiddleware, login)
router.post('/forgot_password', validationsMiddleware, forgotPassword)

module.exports = router
