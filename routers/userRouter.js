const express = require('express')
const router = express.Router()

const { getUserInfo, deleteUser, changeUserPassword} = require('../controllers/userController')

const authMiddleware = require('../middlewares/authMiddleware')

router.get('/users/me', authMiddleware, getUserInfo)
router.delete('/users/me', authMiddleware, deleteUser)
router.patch('/users/me/password', authMiddleware, changeUserPassword)

module.exports = router
