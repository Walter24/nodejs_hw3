const truckTypes = [
  { type: 'SPRINTER',
      dimensions: {
        width: 300,
        length: 250,
        height: 170
      },
      payload: 1700
  },
  {
    type: 'SMALL STRAIGHT',
      dimensions: {
        width: 500,
        length: 250,
        height: 170
      },
      payload: 2500
  },
  {
    type: 'LARGE STRAIGHT',
      dimensions: {
        width: 700,
        length: 350,
        height: 200
      },
      payload: 4000
  }

]

module.exports = truckTypes