const checkShipperRoleMiddleware = async (req, res, next) => {

  const { role } = req.registeredUser

  if (role === 'DRIVER') {
    return next({
      message: 'You have no access to this functionality',
      status: 400
    })
  }
  next()
}

module.exports = checkShipperRoleMiddleware
