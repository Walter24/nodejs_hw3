const accessMiddleware = async (req, res, next) => {

  const { userId, role } = req.registeredUser

  if (role === 'DRIVER') {
    if (userId !== req.truck.created_by) {

      return next({
        message: 'You have no access to this functionality.',
        status: 403
      });
    }
  } else if ((role === 'SHIPPER')) {
    if (userId !== req.load.created_by) {

      return next({
        message: 'You have no access to this functionality.',
        status: 403
      });
    }
  }
  next();
}

module.exports = accessMiddleware;
