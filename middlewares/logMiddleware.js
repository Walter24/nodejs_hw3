const fs = require('fs').promises

const logMiddleware = async (req, res, next) => {

  try {
    const log = `User send request with '${req.method}' method, ${req.url} url
    Time: ${new Date().toISOString()} \n`;
    
    await fs.appendFile('logs.txt', log);

  } catch (err) {
    next(err)
  }
  next()
}

module.exports = logMiddleware
