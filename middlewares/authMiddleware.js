const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {

  const authHeader = req.headers['authorization']
  if(!authHeader) {
    return res.status(401).json({message: 'Input data is not valid. Try again!'})
  }

  const [, jwt_token] = authHeader.split(' ')

  try {
    req.registeredUser = jwt.verify(jwt_token, process.env.JWT_SECRET)
    next()
  } catch (err) {
    return res.status(401).json({message: err.message})
  }
}
