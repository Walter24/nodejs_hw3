const schema = require('../validateModels/authValidModels')

module.exports = async (req, res, next) => {
  try {
    const {email, password, role} = req.body
    await schema.validateAsync({email, password, role})
    next()
  } catch (err) {
    return res.status(500).json({message: err.message})
  }
}