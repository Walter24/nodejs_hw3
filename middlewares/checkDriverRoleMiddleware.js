const checkDriverRoleMiddleware = async (req, res, next) => {
  const { role } = req.registeredUser

  if (role === 'SHIPPER') {
  return next({
      message: 'You have no access to this functionality',
      status: 400
    })
  }
  next()
}

module.exports = checkDriverRoleMiddleware
