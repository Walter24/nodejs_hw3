const express = require('express')
const config = require('config')
const mongoose = require('mongoose')
require('dotenv').config()


const app = express()

const PORT = config.get('port') || 8080

const userRouter = require('./routers/userRouter')
const authRouter = require('./routers/authRouter')
const truckRouter = require('./routers/truckRouter')
const loadRouter = require('./routers/loadRouter')
const logMiddleware = require('./middlewares/logMiddleware')


app.use(express.json())
app.use(logMiddleware)

app.use('/api', userRouter)
app.use('/api', authRouter)
app.use('/api', truckRouter)
app.use('/api', loadRouter)

async function start() {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    })
    app.listen(PORT, () => console.log(`Server has been started on port ${PORT}`))
  } catch(err) {
    console.log('Server error', err.message)
    process.exit(1)
  }
}

start()
