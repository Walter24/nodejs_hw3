const { Schema, model } = require('mongoose')

const schema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String},
  type: {type: String, required: true},
  status: {type: String, required: true},
  created_date: {type: Date, default: Date.now}
})

module.exports = model('Truck', schema)