const { Schema, model } = require('mongoose')

const schema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String},
  status: {type: String, default: 'NEW'},
  state: {type: String, default: 'En route to Pick Up'},
  name: {type: String, required: true},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {
    width: {type: Number, required: true},
    length: {type: Number, required: true},
    height: {type: Number, required: true}
  },
  logs: [
    {
      message: {type: String, required: true},
      time: {type: Date, default: Date.now}
    }
  ],
  created_date: {type: Date, default: Date.now}
})

module.exports = model('Load', schema)